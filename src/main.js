import {createRenderer} from './jirachi/core/gl'
import createCamera from 'perspective-camera'
import Speaker from './objects/speaker'
import Background from './objects/Background'
import {loadAudioSource,reloadBuffer} from './audio/Audio'
import background from './shaders/background.glsl'
// ============= BUILD CORE STUFF =============
// renderer
const gl = createRenderer().setFullscreen().attachToScreen();

// basic camera
const camera = createCamera({
    fov: Math.PI / 4,
    position: [0, 0, 5],
    near: 0.1,
    far: 10000,
    viewport:[0,0,window.innerWidth,window.innerHeight]
})

//============ UI =============


//============ Objects =============//
//var smoke = new SmokeParticles(gl);
var speaker = new Speaker(gl);
var b = new Background(gl,background);
var audioIsPlaying = false;

loadAudioSource(gl,url,(audio,analyser) => {
    window.analyser = analyser;
    setTimeout(function(){
        audio.start(0);
    })
    animate();
});
//oadAudio(gl,url, (audio,analyser) => {
//   window.analyser = analyser;
//   animate();
//)
//============ ANIMATE =============

var time = 0.0;
function animate(){
    time += 0.1;
    window.time = time;

    gl.clearScreen();

    b.draw();
    speaker.draw(camera);
    // eh... not sure what to do with this :p
    //smoke.update();
    //smoke.draw(camera)

    requestAnimationFrame(animate);
}