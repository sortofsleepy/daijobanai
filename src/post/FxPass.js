import {createPingpongBuffer} from '../jirachi/gpu/pingpong'

class FxPass {
    constructor(gl,simulation,{width=window.innerWidth,height=window.innerHeight}={}){
        this.gl = gl;
        this.buffer = createPingpongBuffer(gl,simulation,{
            width:width,
            height:height
        });
    }

    bind(){

    }
}