
precision highp float;


uniform vec2 resolution;
uniform float time;
uniform sampler2D audioTexture;

mat2 mm2(in float a){float c = sin(a), s = cos(a);return mat2(c,-s,s,c);}

float aspect = resolution.x/resolution.y;
float featureSize = 60./((resolution.x*aspect+resolution.y));


varying vec2 uv;
void main(){

    vec2 p = gl_FragCoord.xy / resolution.xy * 4.5 - 3.0;
    vec4 dat = texture2D(audioTexture,gl_FragCoord.xy);


	vec3 yellow = vec3(mix(dat.x,dat.y,1.0) + dat.x,mix(dat.z,dat.x,1.0) + dat.y,sin(time) * cos(dat.x));
	yellow.y += sin( 0.11 * 1.5) * dat.z * cos(time);
	vec2 t = mm2(featureSize) * vec2(1.);
	yellow.xy *= t * sin(time);
	vec2 pa = vec2(abs(yellow.x - .9),abs(yellow.y));
    gl_FragColor =vec4(yellow,dat.z) + vec4(pa,0.0,dat.x);
}
