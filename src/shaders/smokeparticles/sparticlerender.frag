precision highp float;
const vec3 pink = vec3(1.0, 0.07, 0.57);
uniform vec3 particleColor;
varying vec2 vPosition;
uniform sampler2D audioTexture;
void main(){
    vec4 dat = texture2D(audioTexture,vPosition);
    vec3 p = mix(particleColor,dat.xyz + 0.5,1.0);
    gl_FragColor = vec4(p,dat.x) + vec4(particleColor,vPosition.y) + dat;
}