uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;
uniform float theta;
attribute vec3 position;
attribute vec3 offset;
uniform sampler2D pos;
uniform float Time;

uniform sampler2D audioTexture;
varying vec2 vPosition;
void main(){

    // first determine what instance value should be
    vec3 posOffset = position;
    float rnd = noise3d(posOffset);
    // fetch values from our ping ponging
    vec4 calcPos = texture2D(pos,posOffset.xy);

    vec3 finalPos = calcPos.xyz * pow(rnd,4.0);
    vPosition = finalPos.xy;
    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(finalPos,1);
    gl_PointSize = 1.0;
}