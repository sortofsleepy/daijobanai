precision highp float;
uniform sampler2D reflectTexture;
uniform sampler2D brushTexture;
uniform vec3 color;
uniform float opacity;
uniform bool useHue;
uniform float time;
varying float vAngle;
varying vec2 uvCoords;

#define PI 3.14

void main() {
    gl_FragColor = vec4(1.0,1.0,0.0,1.0);
}