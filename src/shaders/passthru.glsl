uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;

varying vec3 vPosition;
attribute vec3 position;

void main(){
    vPosition = position;
    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(position,1.);
}