precision highp float;
uniform vec2 iResolution;
uniform sampler2D amplitude;
varying vec3 vDisplacement;
varying vec3 vPosition;
uniform float time;

// largely based on https://www.shadertoy.com/view/4lB3DG
// by @denzen
void main(){
    vec4 amp = texture2D(amplitude,gl_FragCoord.xy);
    vec2 u = gl_FragCoord.xy;

    u /= iResolution.xy;

    vec3 color = vec3(0.);
    color.xy = .5 - u;
    float t = time;
    float z = atan(color.y,color.x) + 3.;
    float v = cos(z + sin(t * .1)) + 0.5 * cos(u.x * 10. * 1.3) * amp.z;

    color.x = 1.2 + sin(z + t * .2) + sin(u.y*10.+t*1.5)*.5;
 	color.yz = vec2( sin(v*4.)*.25, sin(v*2.)*.3 ) + color.x*.5;
    color.xy * amp.xy * amp.yz;
 	color *= vPosition * vDisplacement;
    gl_FragColor = vec4(color,amp.z);
}