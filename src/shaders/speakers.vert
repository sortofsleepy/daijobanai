
uniform mat4 viewMatrix;
uniform mat4 projectionMatrix;
uniform mat4 modelMatrix;
uniform sampler2D amplitude;

attribute vec3 position;
attribute vec3 displacement;
attribute vec3 normals;
varying vec3 vDisplacement;
varying vec3 vPosition;
void main(){
    vDisplacement = displacement;
    vec4 amp = texture2D(amplitude,position.xy);

    vec3 pos = position + normals * displacement * amp.xyz;


   // vec3 pos = position;
   vPosition = pos;
    gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(pos,1.);
}