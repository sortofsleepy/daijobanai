import mat4 from '../jirachi/math/mat4'
import vec3 from '../jirachi/math/vec3'
import {createVAO} from '../jirachi/core/vao'
import {createVBO} from '../jirachi/core/vbo'
class Mesh {
    constructor(gl){
        this.gl = gl;
        this.model = mat4.create();
        this.rotation = {
            x:0,
            y:0,
            z:0
        };
        this.rotateAxis = vec3.create();
        vec3.set(this.rotateAxis,this.rotation.x,this.rotation.y,this.rotation.z);
        this.scale = vec3.create();
        this.position = vec3.create();
        this.vao = createVAO(gl);
    }

    /**
     * Adds an attribute to the mesh
     * @param shader {Object} either a WebGLShader program or a object created with createShader
     * @param name {String} the name of the attribute in the shader
     * @param data {TypedArray} data associated with the attribute
     */
    addAttribute(shader,name,data){
        let buffer = createVBO(this.gl);
        this.vao.bind();
        buffer.bind();
        buffer.bufferData(data);
        this.vao.addAttribute(shader,name);
        buffer.unbind();
        this.vao.unbind();
    }
    translate(x=1,y=1,z=0){
        vec3.set(this.position,x,y,z);
        mat4.translate(this.model,this.model,this.position);
    }

    scaleModel(x=1,y=1,z=1){
        vec3.set(this.scale,x,y,z);
        mat4.scale(this.model,this.model,this.scale);
    }

    rotateX(angle){
        mat4.rotateX(this.model,this.model,angle);
    }

    rotateY(angle){
        mat4.rotateY(this.model,this.model,angle);
    }

    rotateZ(angle){
        mat4.rotateZ(this.model,this.model,angle);
    }
}

export default Mesh;