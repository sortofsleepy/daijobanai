import {createQuad} from '../jirachi/quad'

// background for everything
class Background {
    constructor(gl,fragment){
        this.gl = gl;
        this.quad = createQuad(gl,{
            fragmentShader:fragment,
            uniformMap:[
                'time',
                'resolution',
                'audioTexture'
            ]
        });

        this.time = 0.0;
        this.resolution = new Float32Array([
            window.innerWidth,
            window.innerHeight
        ])
    }

    draw(){
        this.time += 0.01;
        var gl = this.gl;
        this.gl.enable(this.gl.DEPTH_TEST);
        var resolution = this.resolution;
        gl.enable(gl.BLEND)
        gl.blendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA);

        this.quad.drawWithCallback((shader) => {
            analyser.bindFrequencies(0);
            shader.uniform('time',this.time);
            shader.setTextureUniform('audioTexture',0);
            gl.uniform2fv(shader.getUniform('resolution'),resolution)
        });
    }
}

export default Background;