import Mesh from './Mesh'
//import {createIco} from "./ico"
import {createVBO} from '../jirachi/core/vbo'
import {createShader} from '../jirachi/core/shader'
import {flattenArray} from '../jirachi/math/core'
import {explode} from '../modifier'
import vert from '../shaders/speakers.vert'
import frag from '../shaders/speaker.frag'
import createIco from 'icosphere';
class Speaker extends Mesh{
    constructor(gl){
        super(gl);

        this.shader = createShader(gl,{
            vertex:vert,
            fragment:frag,
            attributes:[
                ['position', 3],
                ['displacement',3],
               // ['normal',3]
            ],
            uniforms:[
                'projectionMatrix',
                'viewMatrix',
                'modelMatrix',
                'amplitude',
                'iResolution',
                'time'
            ]
        });
        this.time = 0.0;
        this.resolution = new Float32Array([
            window.innerWidth,
            window.innerHeight
        ])

        this._build();
    }

    _build(){
        let gl = this.gl;
        let data = createIco(2);

        data = explode(data);
        let positionData = flattenArray(data.positions);
        let normalsData = positionData.slice(0);
        let length = positionData.length;

        // build buffers
        let displacement = createVBO(gl);
        let positions = createVBO(gl);
        let normals = createVBO(gl);
        let idx = createVBO(gl,{
            indexed:true
        });

        this.vao.bind();

        // buffer positions
        positions.bind();
        positions.bufferData(positionData);
        this.vao.addAttribute(this.shader,'position',{
            setData:true
        })

        // buffer indices
        idx.bind();
        idx.bufferData(flattenArray(data.cells));


        displacement.bind();
        var displaceData = [];
        var numFaces = data.cells.length;
        for(var f = 0; f < numFaces;++f){
            var index = 9 * f;
            var d = 10 * ( 0.5 - Math.random() );

            for(var i = 0; i < 3; ++i){
                displaceData[ index + ( 3 * i )     ] = d;
                displaceData[ index + ( 3 * i ) + 1 ] = d;
                displaceData[ index + ( 3 * i ) + 2 ] = d;
            }

        }
        displacement.bufferData(displaceData);
        this.vao.addAttribute(this.shader,'displacement',{
            setData:true
        })

        this.vao.unbind();
        displacement.unbind();
        positions.unbind();
        idx.unbind();
        this.num = data.cells.length * 3;

        // calculate normals for everything
        let normalLength = normalsData.length;
        for(var i = 0; i < normalLength; i+=3){
            normals[i] = Math.random();
            normals[i + 1] = Math.random();
            normals[i + 2] = Math.random();
        }
        this.vao.bind();
        normals.bind();
        normals.bufferData(normalsData);
        this.vao.addAttribute(this.shader,'normals');

        this.vao.unbind();
        normals.unbind();
        // scale the base shape so we can see it.
        this.scaleModel(5,5,5);

    }


    draw(camera){
        var dt = Date.now() * 0.001;
        this.time += 0.1;
        let gl = this.gl;
        gl.disable(gl.DEPTH_TEST);
        this.shader.bind();
        analyser.bindFrequencies(0);
        this.shader.setMatrixUniform('projectionMatrix',camera.projection);
        this.shader.setMatrixUniform('viewMatrix',camera.view);
        this.shader.setMatrixUniform('modelMatrix',this.model);

        this.shader.uniform('time',this.time);
        this.gl.uniform2fv(this.shader.getUniform('iResolution'),this.resolution)
        this.rotateY(0.);
        this.rotateZ(0.02);
        this.rotateX(0.02);

        this.vao.bind();
        this.gl.drawElements(this.gl.TRIANGLES,this.num,UNSIGNED_SHORT,0);
        this.vao.unbind();

        this.gl.bindTexture(TEXTURE_2D,null);
    }

}

export default Speaker;