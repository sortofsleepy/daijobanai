import Mesh from './Mesh'
import {createVBO} from '../jirachi/core/vbo'
import {createVAO} from '../jirachi/core/vao'
import {createShader} from '../jirachi/core/shader'
import {clamp,flattenArray,isVector} from '../jirachi/math/core'
import r from '../ramda'
import mat4 from '../jirachi/math/mat4'

import vertex from '../shaders/soundline/vertex.glsl'
import fragment from '../shaders/soundline/fragment.glsl'
import noise from '../jirachi/shaders/noise3d.glsl'
class LineMesh extends Mesh {
    constructor(gl){
        super(gl);
        let thickness = 1
        let aspect = 1
        let miter = 0
        let color = [1, 1, 1]

        this.lines = [];
        this.shader = createShader(gl,{
            vertex:[noise,vertex],
            fragment:fragment,
            uniforms:[
                'projection',
                'model',
                'view',
                'aspect',
                'radius',
                'index',
                'thickness',
                'time',
                'miter'
            ]
        })

        this.scaleModel(30,30,30)

        this._initLine();
    }
    draw(camera){
        let gl = this.gl
        let lines = this.lines;
        this.rotateX(0.02);
        this.rotateZ(0.02);
        gl.enable(gl.DEPTH_TEST);
        lines.forEach((line,i,list) => {
            let shader = line.shader;
            let vao = line.line.vao;


            shader.bind();

            // bind uniforms
            shader.uniform('thickness',0.1)
            shader.setMatrixUniform('model',line.model);
            shader.setMatrixUniform('view',camera.view);
            shader.setMatrixUniform('projection',camera.projection)
            shader.uniform('aspect',window.innerWidth / window.innerHeight)
            shader.uniform('index',i / (list.length - 1));


            vao.bind();
            gl.drawElements(gl.TRIANGLE,line.line.count,UNSIGNED_SHORT,0);
            vao.unbind();

        });
    }
    _initLine(){
        let paths = r.range(0,100).map(this._createCirc);

        paths.map(path => {
            this.lines.push({
                line:this._buildLine(path),
                shader:this.shader,
                model:this.model
            });
        });

    }

    /**
     * Builds each individual line segment
     * @param path
     * @returns {{gl, vao, ext, attributes, setAttributeLocation, enableAttributes, addAttribute, getAttribute, enableAttribute, disableAttribute, setData, point, bind, unbind}|*}
     * @private
     */
    _buildLine(path){
        let gl = this.gl;
        path = isVector(path);
        let vao = createVAO(gl);

        let count = (path.length - 1) * 6


        let direction = this._duplicate(path.map(x => 1), true)
        // now get the positional data for each vertex
        let positions = this._duplicate(path)
        let previous = this._duplicate(path.map(this._relative(-1)))
        let next = this._duplicate(path.map(this._relative(+1)))
        let indexUint16 = this._createIndices(path.length)

        // build buffer
        let position = createVBO(gl);
        let directionBuffer = createVBO(gl);
        let nextBuffer = createVBO(gl);
        let previousBuffer = createVBO(gl);
        let indices = createVBO(gl,{
            indexed:true
        });

        vao.bind();
        position.bind();
        directionBuffer.bind();
        nextBuffer.bind();
        previousBuffer.bind();
        indices.bind();


        // set positions
        position.bufferData(flattenArray(positions));
        directionBuffer.bufferData(direction);
        nextBuffer.bufferData(flattenArray(next));
        previousBuffer.bufferData(flattenArray(previous));
        indices.bufferData(indexUint16);

        vao.addAttribute(this.shader,'position');
        vao.addAttribute(this.shader,'direction',{
            size:1
        });
        vao.addAttribute(this.shader,'next');
        vao.addAttribute(this.shader,'previous');


        vao.unbind();
        position.unbind();
        directionBuffer.unbind();
        nextBuffer.unbind();
        previousBuffer.unbind();

        return {
            vao:vao,
            count:count
        }

    }



    _relative(offset){
        return (point, index, list) => {
            index = clamp(index + offset, 0, list.length - 1)
            return list[index]
        }

    }
    _duplicate(nestedArray,mirror){
        var out = []
        nestedArray.forEach(x => {
            let x1 = mirror ? -x : x
            out.push(x1, x)
        })
        return out

    }
    _createCirc(){
        var deg2rad = 3.14149 / 180;
        var radius = Math.random() * 2.0;

        var comps = [];

        for(var i = 0; i < 360; ++i){
            var deg = i * deg2rad;
            var x = Math.cos(deg) * radius;
            var y = Math.sin(deg) * radius;
            var z = 0.0;

            //normalize values
            var len = x * x + y * y + z * z;
            if(len > 0){
                len = 1 / Math.sqrt(len);
                x *= len;
                y *= len;
                z *= len;
            }

            comps.push([x,y,z]);
        }

        return comps;
    }

    _createIndices(length){
        let indices = new Uint16Array(length * 6)
        let c = 0
        let index = 0
        for (let j = 0; j < length; j++) {
            let i = index
            indices[c++] = i + 0
            indices[c++] = i + 1
            indices[c++] = i + 2
            indices[c++] = i + 2
            indices[c++] = i + 1
            indices[c++] = i + 3
            index += 2
        }
        return indices

    }
}

export default LineMesh;