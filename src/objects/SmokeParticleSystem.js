import Mesh from './Mesh'
import {createVBO} from '../jirachi/core/vbo'
import {createShader} from '../jirachi/core/shader'
import {createTexture2d} from '../jirachi/core/texture'
import {createMultiPingpongBuffer,generatePingpongTexture} from '../jirachi/gpu/pingpong'
import SimplexNoise from '../jirachi/math/SimplexNoise'
import noise3d from '../jirachi/shaders/noise3d.glsl'
// import shaders
import sim from '../shaders/smokeparticles/smokeparticle.glsl'
import rvertex from '../shaders/smokeparticles/sparticlerender.vert'
import rfragment from '../shaders/smokeparticles/sparticlerender.frag'
import rotation from '../jirachi/shaders/rotation.glsl'
class SmokeParticleSystem extends Mesh {
    constructor(gl){
        super(gl);

        this.instanced = gl.ANGLE_instanced_arrays;

        this.shader = createShader(gl,{
            vertex:[noise3d,rotation,rvertex],
            fragment:rfragment,
            attributes:[
                ['position',3],
                ['offset',3]
            ],
            uniforms:[
                'projectionMatrix',
                'modelMatrix',
                'viewMatrix',
                'Time',
                'pos',
                'audioTexture',
                'particleColor'
            ]
        });

        this.theta = 0.0;
        this.size = 768.0;
        this.s2 = this.size * this.size;
        this.time = 0.0;
        this.rand = new SimplexNoise();
        this._buildAttribs();
        //this._buildBaseShape();
        //this._buildPositions();
        this._testParticles();
        //this.currentColor = new Float32Array(3);
        this._enableUI();
        this.currentColor = [1.0,0.0,0.0]
        this.scaleModel(10,10,10);
    }


    //====== UI =============
    _changeColor(c){
        this.currentColor = c;
    }
    _enableUI(){
        let ui = window.ui;

        var particles = ui.addPanel({
            label:'ParticleSystem'
        });
        this.color = {
            "mix color":[1,0,0]
        }
        particles.addColor(this.color,'mix color',{
            colorMode:'rgb',
            onChange:this._changeColor.bind(this)
        })


    }

    _buildAttribs(){
        let gl = this.gl;
        let size = this.s2 * 4;
        let baseArray = new Float32Array(size);
        // setup positions
        let positionTex = generatePingpongTexture(gl,baseArray,{
            width:this.size,
            height:this.size
        });


        //setup normals, we'll reuse the positions array
        let normals = new Float32Array(size);
        let len = normals.length;
        let radius = 20;
        for(var i = 0; i < len ;++i){
            let n = this.rand.noise(0.0,1.5) * radius;
            normals[i] = (Math.random() - 0.5 ) * n;
            normals[i + 1] =(Math.random() - 0.5 ) * n
            normals[i + 2] =  (Math.random() - 0.5 ) * n;
            normals[i + 3] =  (Math.random() - 0.5 ) * n;
        }
        let velocityTex = generatePingpongTexture(gl,normals,{
            width:this.size,
            height:this.size
        });

        // a texture that can be used to reset things when a particle is dead
        let initVelocity = createTexture2d(gl,{
            width:this.size,
            height:this.size,
            data:normals,
            textureOptions:{
                type:FLOAT
            }
        })

        let timeData = new Float32Array(size);
        let time = 0;
        let rate = 0.001;
        // it's only a float value, so we only need to store something in
        // the red channel
        for(var i = 0; i < size; i += 4){
            timeData[i] = time;
            time += rate;
        }
        let timeTex = generatePingpongTexture(gl,timeData,{
            width:this.size,
            height:this.size
        });

        // generate ping pong-ing buffer
        this.buffer = createMultiPingpongBuffer(gl,sim,{
            width:this.size,
            height:this.size,
            textureMap:[
                positionTex,
                velocityTex,
                timeTex
            ],
            uniformMap:[
                'pos',
                'vel',
                'startTime',
                'Time',
                'initVelocity',
                'audioTexture'
            ]
        })

        this.initVelocity = initVelocity
    }


    _testParticles(){
        let gl = this.gl;
        let position = createVBO(gl);
        let vertices = new Float32Array(this.s2 * 4);
        let len = vertices.length;
        for(var i = 0; i < len; ++i){
            vertices[i] = Math.random();
        }
        this.vao.bind();
        position.bind();
        position.bufferData(vertices);
        this.vao.addAttribute(this.shader,'position',{
            setData:true
        });
        position.unbind();
        this.vao.unbind();
        this.num = vertices.length / 3;
    }
    update(){
        this.rotateY(0.02)
        let gl = this.gl;
        this.time += 1.0;
        this.theta += 200.1;
        gl.disable(gl.BLEND)
        this.buffer.update((shader) => {
            shader.bind();
            this.initVelocity.bind(3);
            analyser.bindFrequencies(4)
            shader.uniform("Time",this.time / 60.0);
            shader.setTextureUniform('pos',0);
            shader.setTextureUniform('vel',1);
            shader.setTextureUniform('startTime',2);
            shader.setTextureUniform('initVelocity',3);
            shader.setTextureUniform('audioTexture',4);

        });
    }

    draw(camera){
        let gl = this.gl;
        this.gl.bindTexture(TEXTURE_2D,null);

        gl.disable(gl.DEPTH_TEST)
        gl.enable(gl.BLEND)
        gl.blendFunc(gl.ONE, gl.ONE_MINUS_SRC_ALPHA);

        this.shader.bind();

        analyser.bindFrequencies(1)
        // remember - this occupies texture slots 0-2
        this.buffer.bindTextures();
        this.shader.uniform('projectionMatrix',camera.projection);
        this.shader.uniform('viewMatrix',camera.view);
        this.shader.uniform('modelMatrix',this.model);
        this.shader.uniform('theta',this.theta);
        this.shader.uniform('Time',this.time);
        this.shader.uniform('particleColor',this.currentColor);
        this.shader.setTextureUniform('pos',0);
        this.shader.setTextureUniform('audioTexture',1);



        this.vao.bind();
        //this.gl.drawInstancedArrays(TRIANGLES,0,3,128 * 128);
        this.gl.drawArrays(POINTS,0,this.num);
        this.vao.unbind();

    }
}

export default SmokeParticleSystem;