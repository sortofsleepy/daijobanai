
/**
 * Load audio file for playback
 * @param path path to file
 * @param cb callback for when it's ok to start playing the audio
 */

export function loadAudio(gl,path,cb){
    var audio = document.createElement('audio');
    audio.crossOrigin = 'Anonymous';
    audio.controls = true;

    document.body.appendChild(audio);


    if(path.search('http') === -1){
        audio.src = path;
        audio.addEventListener('canplay',() => {
            var analyser = GLAudioAnalyser(gl,audio);
            cb(audio,analyser);
        })
    }else{
        // this is basically soundcloud-resolve but broken down and less robust :p
        // https://github.com/hughsk/soundcloud-resolve/blob/master/index.js
        const client_id = "b36fa0e8aa765329c8bad7c78e01b31a"
        const soundcloud = `https://api.soundcloud.com/resolve.json?client_id=${client_id}&url=${path}`
        var req = new XMLHttpRequest();
        req.open('GET',soundcloud);

        req.onreadystatechange = () => {
            if(req.readyState === 4){
                if(req.status === 403){
                    alert("There appears to be an issue with streaming this track, please enter another track url")
                }
                if(req.status === 200){
                    var data = JSON.parse(req.responseText);
                    var stream_url = data.kind === 'track'
                        && data.stream_url + '?client_id=' + client_id

                    audio.src = stream_url;
                    audio.addEventListener('canplay',() => {
                        var analyser = GLAudioAnalyser(gl,audio);
                        cb(audio,analyser);
                    })
                }
            }
        }

        req.send();

    }
}

export function reloadBuffer() {
    let audioCtx = new (window.AudioContext || window.webkitAudioContext);
    let source = audioCtx.createBufferSource();

    source.buffer = audioBuffer;
    source.connect(audioCtx.destination);

    return source;
}

export function loadAudioSource(gl,url,cb){
    let audioCtx = new (window.AudioContext || window.webkitAudioContext);
    let req = new XMLHttpRequest();

    let source = audioCtx.createBufferSource()

    req.open('POST',url,true);

    req.setRequestHeader('Content-type','application/x-www-form-urlencoded');
    req.responseType = 'arraybuffer';
    req.onload = () => {
        let audioData = req.response;
        audioCtx.decodeAudioData(audioData,buffer => {
            // this is hacky but necessary for play/pause
            window.audioBuffer = buffer;

            source.buffer = buffer;
            source.connect(audioCtx.destination);

            var analyser = GLAudioAnalyser(gl,source,audioCtx);
            cb(source,analyser);
        },e => {
            console.log("error decoding - ",e.err);
        })
    }

    req.send();
}