/**
 * Flattens an nested array that is assumed to be nested with child arrays used in place of
 * an actual vector object. Note, this does not check for completeness and will automatically
 * only take the first 3 values of the child arrays
 * @param array the parent array
 * @returns {Array}
 */
export function flattenArray(array){

    let fin = [];
    let len = array.length;
    for(var i = 0; i < len; ++i){
        let arr = array[i];
        fin.push(arr[0],arr[1],arr[2])
    }
    return fin;
}

/**
 * Does subtraction between two arrays. Assumes both arrays have 3 values each inside
 * @param array1 {Array} the array to subtract from
 * @param array2 {Array} the array to subtract
 * @returns {*[]}
 */
export function subArrays(array1,array2){

    let x1 = array1[0];
    let y1 = array1[1];
    let z1 = array1[2];

    let x2 = array2[0];
    let y2 = array2[1];
    let z2 = array2[2];
    return [x1 - x2, y1 - y2, z1 - z2];
}



export function normalizeArray(value){
    var length = Math.sqrt(value[0] * value[0] + value[1] * value[1] + value[2] * value[2]);
    return [
        value[0] / length,
        value[1] / length,
        value[2]/ length
    ]
}

export function cross(a,b){

    let a1 = a[0];
    let a2 = a[1];
    let a3 = a[2];
    let b1 = b[0];
    let b2 = b[1];
    let b3 = b[2];

    return [a2 * b3 - a3 * b2, a3 * b1 - a1 * b3, a1 * b2 - a2 * b1]
}

/**
 * Creates an array with a range of values
 * @param from {Number} the value to start from
 * @param to {Number} the value end at.
 * @returns {Array}
 */
export function range(from,to){
    var result = [];
    var n = from;
    while (n < to) {
        result.push(n);
        n += 1;
    }
    return result;
}

/**
 * Returns a random vec3(in the form of an array)
 * @returns {*[]}
 */
export function randVec3(){
    return new Float32Array([
        Math.random(),
        Math.random(),
        Math.random()
    ]);
}

/**
 * Very simple array cloning util.
 * Note - only works with arrays who have 3 elements
 * @param arrayToClone the array to clone
 * @returns {*[]} the new array
 */
export function cloneArray(arrayToClone){
    return [
        arrayToClone[0],
        arrayToClone[1],
        arrayToClone[2]
    ]
}

export function clamp(value,min,max){
    return min < max
        ? (value < min ? min : value > max ? max : value)
        : (value < max ? max : value > min ? min : value)
}

/**
 * ensures that when using an array as a 3d vector, that it actually
 * contains only 3 components.
 * @param array the array to verify
 * @returns {*}
 */
export function isVector(array){
    // ensure 3 component vectors
    if (array.length > 0 && array[0].length !== 3) {
        array = array.map(point => {
            let [x, y, z] = point
            return [x || 0, y || 0, z || 0]
        })
    }
    return array;
}