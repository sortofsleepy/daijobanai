import passthru from './shaders/passthru.glsl'
import passthru_frag from './shaders/passthru-frag.glsl'
import {createRenderer} from './core/gl'
import {makeShaderSpec} from './core/Shader'
const raf = require('raf');

var renderer = createRenderer()
    .setFullscreen()
    .attachToScreen(document.body);

let shader = makeShaderSpec(renderer,{
    vertex:passthru,
    fragment:passthru_frag,
    attributes:[
        ["position",3],
        ["color",4]
    ],
    uniforms:[
        "projectionMatrix"
    ]
});












