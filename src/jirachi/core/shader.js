import {logError} from '../utils'

/**
 * Compiles either a fragment or vertex shader
 * @param gl a webgl context
 * @param type the type of shader. Should be either gl.FRAGMENT_SHADER or gl.VERTEX_SHADER
 * @param source the source (as a string) for the shader
 * @returns {*} returns the compiled shader
 */
export function compileShader(gl,type,source){
    let shader = gl.createShader(type);

    gl.shaderSource(shader, source);
    gl.compileShader(shader);

    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        console.error("Error in shader compilation - " + gl.getShaderInfoLog(shader));
        return false;
    } else {
        return shader;
    }
}

/**
 * The main function for creating a shader. Shader also manages figuring out
 * attribute and uniform location indices.
 *
 * @param gl a webgl context
 * @param vertex the source for the vertex shader
 * @param fragment the source for the fragment shader
 * @returns {*} returns the WebGLProgram compiled from the two shaders
 */
export function makeShader(gl,vertex,fragment){
    let vShader = compileShader(gl,gl.VERTEX_SHADER,vertex);
    let fShader = compileShader(gl,gl.FRAGMENT_SHADER,fragment);

    if(vShader !== false && fShader !== false){
        let program = gl.createProgram();
        gl.attachShader(program,vShader);
        gl.attachShader(program,fShader);
        gl.linkProgram(program);

        if(!gl.getProgramParameter(program,gl.LINK_STATUS)){
            logError("Could not initialize WebGLProgram");
            throw ("Couldn't link shader program - " + gl.getProgramInfoLog(program));
            return false;
        }else{
            return program;
        }
    }
}
/**
 * A function to quickly setup a WebGL shader program.
 * Modeled a bit after thi.ng
 * @param gl the webgl context to use
 * @param spec a object containing the out line of what the shader would look like.
 * @returns {*} and JS object with the shader information along with some helpful functions
 */
export function createShader(gl=null,spec){
    let vs = null;
    let fs = null;
    let uniforms = {};
    let attributes = {};
    let precision = spec.precision !== undefined ? spec.precision : "highp";
    if(gl === null){
        console.error("")
        return false;
    }

    if(!spec.hasOwnProperty("vertex") || !spec.hasOwnProperty("fragment")){
        logError("spec does not contain vertex and/or fragment shader");
        return false;
    }

    // if either of the shader sources are arrays, run the compile shader function
    if(spec.vertex instanceof Array){
        spec.vertex = compileShaderSource(spec.vertex);
    }

    if(spec.fragment instanceof Array){
        spec.fragment = `precision ${highp} float;` + compileShaderSource(spec.fragment);
    }

    // build the shader
    let shader = makeShader(gl,spec.vertex,spec.fragment);

    // set uniforms and their locations (plus default values if specified)
    if(spec.hasOwnProperty('uniforms')){

        let uValues = spec.uniforms.map((value) => {
            if(typeof value === 'string'){
                let loc = gl.getUniformLocation(shader,value);
                uniforms[value] = loc;
            }else if (typeof value === 'object'){
                // TODO make sure to set default uniform value if present
                let loc = gl.getUniformLocation(shader,value.name);
                uniforms[value.name] = loc;
            }
        })

    }

    /**
     * Arranges all of the attribute data into neat containers
     * to allow for easy processing by a VAO.
     * Attributes should be specified as arrays
     */
    if(spec.hasOwnProperty('attributes')){
        let attribs = spec.attributes.map((value) => {

            attributes[value[0]] = {
                size:value[1],
                name:value[0]
            };

            // if a desired uniform location is set ,
            // make sure to reflect that in the information
            if(value[2] !== undefined){
                attributes[value[0]].location = value[2];
            }
        });
    }

    return {
        gl:gl,
        program:shader,
        uniforms:uniforms,
        attributes:attributes,
        /**
         * Binds the shader for use
         */
        bind(){
          this.gl.useProgram(this.program);
        },
        /**
         * Sets a matrix uniform for a 4x4 matrix
         * @param name the name of the uniform whose value you want to set.
         */
        setMatrixUniform(name,value){

            this.gl.uniformMatrix4fv(this.uniforms[name],false,value);
        },
        /**
         * Sets the uniform value for a texture. Optionally
         * @param value
         */
        setTextureUniform(name,value){
            this.gl.uniform1i(this.uniforms[name],value);
        },

        getUniform(name){
            return this.uniforms[name];
        },

        /**
         * Sends a uniform to the currently bound shader. Attempts to derrive
         * the correct uniform function to use
         * @param name {String} name of the uniform
         * @param value {*} the value to send to the uniform
         */
        uniform(name,value){

            // if the value is a 4x4 matrix (assuming from gl-matrix)
            if(value.length !== undefined && value.length === 16){
                this.setMatrixUniform(name,value);
            } else if(value.length !== undefined && value.length === 3){
                this.gl.uniform3fv(this.uniforms[name],value);
            }else {
                // send a float based value
                this.gl.uniform1f(this.uniforms[name],value);
            }

        }

    }
}


/**
 * Allows you to compile multiple shader sources into one file.
 * Keep in mind this does not distinguish between vertex and fragment and will not
 * insert things like precision specifiers and/or extensions
 * @param sources
 */
export function compileShaderSource(...sources){
    if(sources[0] instanceof Array){
        var s = sources[0].map((source) => {
            return source + "\n";
        });
        return s.join("");
    }else{
        var s = sources.map((source) => {
            return source + "\n";
        });
        return s.join("");
    }
}