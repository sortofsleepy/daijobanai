import {createTexture2d} from "./texture"
import {log} from '../utils'
export function createFBOWithAttachments(gl,num=1,{textures,width=512,height=512,floatingPoint=false,uniformMap}={}){
    textures = textures || false;

    let attachments = [];
    let framebuffer = gl.createFramebuffer();

    // get the max number of color attachments.
    // we know 0 will already be occupied.
    let ext = gl.WEBGL_draw_buffers;
    let maxAttachments = ext.MAX_COLOR_ATTACHMENTS_WEBGL;

    if(textures instanceof Array && textures.length > 0){

        if(textures.length > maxAttachments){
            console.error("The number of textures passed in appears to exceed the max number of attachments")
        }else{
            gl.bindFramebuffer(FRAMEBUFFER, framebuffer);
            // append textures to framebuffer
            for (var i = 0; i < textures.length; ++i) {

                // attach primary drawing textures
                gl.framebufferTexture2D(FRAMEBUFFER, ext[`COLOR_ATTACHMENT${i}_WEBGL`], TEXTURE_2D, textures[i].texture, 0);
                attachments.push(ext[`COLOR_ATTACHMENT${i}_WEBGL`]);
            }
            ext.drawBuffersWEBGL(attachments);
            gl.bindFramebuffer(FRAMEBUFFER, null);

        }
    }else {
        textures = [];
        // if floating point is true, we create a FBO with
        // a floating point texture(meaning, a texture that can store floating point numbers), otherwise, just create a regular fbo
        // with integer based information
        if (floatingPoint) {
            for (var i = 0; i < num; ++i) {
                textures.push(createTexture2d(gl, {
                    width: width,
                    height: height,
                    textureOptions: {
                        type: FLOAT
                    }
                }));
            }
        } else {
            for (var i = 0; i < num; ++i) {
                textures.push(createTexture2d(gl,{
                    width:width,
                    height:height
                }));
            }
        }


        // append textures to framebuffer
        for (var i = 0; i < num; ++i) {
            // attach primary drawing texture.
            // TODO change to WebGL 2 code later on
            gl.bindFramebuffer(FRAMEBUFFER, framebuffer);
            gl.framebufferTexture2D(FRAMEBUFFER, ext[`COLOR_ATTACHMENT${i}_WEBGL`], TEXTURE_2D, textures[i].texture, 0);
            gl.bindFramebuffer(FRAMEBUFFER, null);
            attachments.push(ext[`COLOR_ATTACHMENT${i}_WEBGL`]);
        }
    }




    return {
        gl:gl,
        textures:textures,
        fbo:framebuffer,
        attachments:attachments,
        ext:ext,
        maxDrawBuffers:ext.MAX_DRAW_BUFFERS_WEBGL,
        bindBuffers(){
            this.bindFbo();
            this.ext.drawBuffersWEBGL(this.attachments);
            this.unbindFbo();
        },
        /**
         * For binding the Fbo to draw onto it.
         */
        bindFbo(){
            gl.bindFramebuffer(FRAMEBUFFER,framebuffer);
        },
        /**
         * Unbinds the previously bound FBO, returning drawing commands to
         * the main context Framebuffer.
         */
        unbindFbo(){
            gl.bindFramebuffer(FRAMEBUFFER,null);
        },
        /**
         * Binds all the textures on the framebuffer.
         * the order is determined by the order of creation.
         */
        bindTextures(){
            let len = this.textures.length;
            for(var i = 0; i < len; ++i){
                this.textures[i].bind(i);
            }
        },
        /**
         * Binds the texture of the framebuffer
         * @param index the index to bind the texture to
         */
        bindTexture(index=0){
            this.textures[index].bind(index);
        },
        /**
         * Unbinds the framebuffer's texture
         */
        unbindTexture(index=0){
            this.textures[index].unbind();
        }
    }
}


/**
 * Creates a WebGL Framebuffer object.
 * @param gl {WebGLRenderingContext} a WebGlRenderingContext
 * @param width {Number} the width for the fbo
 * @param height {Number} the height for the number
 * @param floatingPoint {Bool} whether or not the FBO should store floating point information
 * @returns {{gl: *, drawTexture: *, fbo: *, bindFbo: bindFbo, unbindFbo: unbindFbo, bind: bind, unbind: unbind}}
 */
export function createFBO(gl,{width,height,floatingPoint,texture=null}={}){
    width = width || 512;
    height = height || 512;
    floatingPoint = floatingPoint || false

    let framebuffer = gl.createFramebuffer();
    let t = null;

    if(texture !== null){
        t = texture;
    }else{
        // if floating point is true, we create a FBO with
        // a floating point texture(meaning, a texture that can store floating point numbers), otherwise, just create a regular fbo
        // with integer based information
        if(floatingPoint){
            t = createTexture2d(gl,{
                width:width,
                height:height,
                textureOptions:{
                    type:FLOAT
                }
            });
        }else{
            t = createTexture2d(gl);
        }
    }
    // attach primary drawing texture.
    gl.bindFramebuffer(FRAMEBUFFER,framebuffer);
    gl.framebufferTexture2D(FRAMEBUFFER,COLOR_ATTACHMENT0,TEXTURE_2D,t.texture,0);
    gl.bindFramebuffer(FRAMEBUFFER,null);

    return {
        gl:gl,
        drawTexture:t,
        fbo:framebuffer,
        /**
         * For binding the Fbo to draw onto it.
         */
        bindFbo:function(){
            gl.bindFramebuffer(FRAMEBUFFER,framebuffer);
        },

        /**
         * Unbinds the previously bound FBO, returning drawing commands to
         * the main context Framebuffer.
         */
        unbindFbo:function(){
            gl.bindFramebuffer(FRAMEBUFFER,null);
        },

        /**
         * Binds the texture of the framebuffer
         * @param index the index to bind the texture to
         */
        bindTexture:function(index=0){
            this.drawTexture.bind(index);
        },

        /**
         * Unbinds the framebuffer's texture
         */
        unbindTexture:function(){
            this.drawTexture.unbind();
        }
    }
}