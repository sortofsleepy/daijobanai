/**
 * A stand alone function for creating data based textures with TypedArrays.
 * Usable on it's own, but recommended that you use the {@link createTexture2d}
 * function
 * @param gl {WebGLRenderingContext} a WebGLRenderingContext
 * @param data {TypedArray} a TypedArray of data you want to write onto the texture
 * @param options {Object} a map of options for the texture creation
 * @returns {*}
 */
export function createDataTexture(gl,data,options){
    let texture = gl.createTexture();

    gl.bindTexture(TEXTURE_2D,texture);
    gl.texImage2D(
        TEXTURE_2D,
        0,
        options.internalFormat,
        options.width,
        options.height,
        0,
        options.format,
        options.type,
        data
    );

    // set min and mag filters
    gl.texParameteri(TEXTURE_2D,MAG_FILTER,options.magFilter);
    gl.texParameteri(TEXTURE_2D,MIN_FILTER,options.minFilter)

    //set wrapping
    gl.texParameteri(TEXTURE_2D,WRAP_S,options.wrapS)
    gl.texParameteri(TEXTURE_2D,WRAP_T,options.wrapT)

    // generate mipmaps if necessary
    if(options.generateMipMaps){
        gl.generateMipmap(TEXTURE_2D);
    }

    gl.bindTexture(TEXTURE_2D,null);

    return texture;
}

export function resizeFbo(gl,fbo){

}

/**
 * Create an image based texture. Usable on it's own, but recommended that you use the {@link createTexture2d}
 * function
 * @param gl {WebGLRenderingContext} a WebGLRenderingContext
 * @param image {Image} and image object
 * @param options {Object} a map of options for the texture creation
 * @returns {*}
 */
export function createImageTexture(gl,image,options){
    let texture = gl.createTexture();
    gl.bindTexture(TEXTURE_2D,texture);

    // set the image
    gl.texImage2D(TEXTURE_2D,0,options.format,options.format,options.type,image);

    // set min and mag filters
    gl.texParameteri(TEXTURE_2D,MAG_FILTER,options.magFilter);
    gl.texParameteri(TEXTURE_2D,MIN_FILTER,options.minFilter)

    //set wrapping
    gl.texParameteri(TEXTURE_2D,WRAP_S,options.wrapS)
    gl.texParameteri(TEXTURE_2D,WRAP_T,options.wrapT)

    // generate mipmaps if necessary
    if(options.generateMipMaps){
        gl.generateMipmap(TEXTURE_2D);
    }

    gl.bindTexture(TEXTURE_2D,null);

    return texture;
}


/**
 * Simple function for creating a 2D texture
 * @param gl a WebGLRendering context
 */
export function createTexture2d(gl,{data,textureOptions,width,height}={}){
    let texture = null;

    // NOTES
    // 1. in WebGL 1 , internalFormat and format ought to be the same value.
    // 2. UNSIGNED_BYTE corresponds to a Uint8Array, float corresponds to a Float32Array
    let defaults = {
        format:RGBA,
        internalFormat:RGBA,
        type:UNSIGNED_BYTE,
        wrapS:CLAMP_TO_EDGE,
        wrapT:CLAMP_TO_EDGE,
        minFilter:LINEAR,
        magFilter:LINEAR,
        generateMipMaps:false
    }

    if(textureOptions !== undefined){
        Object.assign(defaults,textureOptions);
    }

    // if we have data, process it as such, otherwise generate a blank texture of random data
    if(data === undefined){
        width = width || 128;
        height = height || 128;

        let data = null;

        // if textureOptions isn't undefined, check to see if we've defined the "type" key.
        // if that is set to the floating point constant, make sure to use a Float32Array,
        // otherwise default to Uint8Array.
        // If the parameter isn't defined, default to Uint8Array
        if(textureOptions !== undefined){
            if(textureOptions.hasOwnProperty('type')){
                if(textureOptions.type === FLOAT){
                    data = new Float32Array(width * height * 4);
                }else{
                    data = new Uint8Array(width * height * 4);
                }
            }
        }else{
            data = new Uint8Array(width * height * 4);
        }

        for(var i = 0; i < (width * height * 4);i += 4){
            data[i] = Math.random();
            data[i + 1] = Math.random();
            data[i + 2] = Math.random();
            data[i + 3] = 1.0;
        }

        defaults["width"] = width;
        defaults["height"] = height;
        texture = createDataTexture(gl,data,defaults);

        // if we have data
    }else{
        defaults["width"] = width || 128;
        defaults["height"] = height || 128;

        // if it's an image, build an image texture
        if(data instanceof Image){
            texture = createImageTexture(gl,data,defaults);
        }

        // if it's a float 32 array we, build a data texture.
        if(data instanceof Float32Array){
            if(defaults.type !== FLOAT){
                defaults.type = FLOAT;
            }
            texture = createDataTexture(gl,data,defaults);
        }

        // if it's a float 32 array we, build a data texture.
        if(data instanceof Uint8Array){
            if(defaults.type !== FLOAT){
                defaults.type = FLOAT;
            }
            texture = createDataTexture(gl,data,defaults);
        }

        if(data instanceof Array){
            if(defaults.type !== FLOAT){
                defaults.type = FLOAT;
            }
            texture = createDataTexture(gl,new Float32Array(data),defaults);
        }

    }

    return {
        gl:gl,
        texture:texture,
        getTexture(){
            return this.texture;
        },
        bind(index=0){
            let gl = this.gl;
            gl.activeTexture(TEXTURE0 + index);
            gl.bindTexture(TEXTURE_2D,this.texture);


            this.isBound = true;
        }
    }
}
