// port of the Three.js explode modifier.
export function explode(geo){
    var vertices = [];

    for ( var i = 0, il = geo.cells.length; i < il; i ++ ) {

        var n = vertices.length
        var face = geo.cells[ i ];
        var a = face[0];
        var b = face[1];
        var c = face[2];

        var va = geo.positions[ a ];
        var vb = geo.positions[ b ];
        var vc = geo.positions[ c ];


        vertices.push( [va[0],va[1],va[2]] );
        vertices.push( [vb[0],vb[1],vb[2]] );
        vertices.push( [vc[0],vc[1],vc[2]] );

        face[0] = n;
        face[1] = n + 1;
        face[2] = n + 2;
    }

    geo.positions = vertices;
    return geo;
}