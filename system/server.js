var http = require('http');
var stacked = require('stacked');
var serveStatic = require('serve-static');
var finalHandler = require('finalhandler');
var chokidar = require('chokidar');
var pkg = require('./pkg');
var bole = require('bole');
var log = require('bole')('server');
var liveReload = require('./inject')
var garnish = require('garnish');
var WebSocketServer = require('ws').Server;

//============ SETUP ================
var pretty = garnish({
    level:'info',
    name:'daijobani'
})
pretty.pipe(process.stdout);
bole.output({
    level:'info',
    stream:pretty
});
// port
const PORT = 3002;

var wss = new WebSocketServer({port:3001});


//============ SERVER ================
var app = stacked();
app.use(liveReload());
// set up serve func
var serve = serveStatic(__dirname + '/../public',{
    index:['index.html']
});

app.use(serve);

var watcher = chokidar.watch(__dirname + '/../src');
var server = http.createServer(app);
server.listen(PORT,function(){
    log.info(`Server started, listening on port ${PORT}`);
});

//============ FILE WATCHING ================

var socket = null;
wss.on('connection',function connection(ws){
    socket = ws;
});

watcher.on('change',(path) => {
    var s = pkg(__dirname + "/../src/main.js");
    s.then(function(e){
        if(socket !== null){
            socket.send("reload")
        }
    });
});