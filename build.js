const rollup = require('rollup');
const babel = require('rollup-plugin-babel');
var fs = require('fs');
const uglifyjs = require('uglify-js')
var nodeResolve = require('rollup-plugin-node-resolve');
var commonjs = require('rollup-plugin-commonjs');
function glsl () {
    return {
        transform ( code, id ) {
            if ( !/\.(glsl|frag|vert)$/.test( id ) ) return;

            return 'export default ' + JSON.stringify(
                    code
                        .replace( /[ \t]*\/\/.*\n/g, '' )
                        .replace( /[ \t]*\/\*[\s\S]*?\*\//g, '' )
                        .replace( /\n{2,}/g, '\n' )
                ) + ';';
        }
    };
}



function bundle(src,dest){
    return rollup.rollup({
        entry:src,
        plugins:[
            glsl(),
            nodeResolve(),
            commonjs(),
            babel({
                exclude:'../node_modules/**'
            })
        ]
    }).then((bundle) => {
        return bundle.write({
            format:'umd',
            moduleName:'jra',
            dest:dest
        })
    }).catch(err => {
        console.log(err);
    })
}
//======== START PROCESS ==========
const src = './src/main.js';
const dest = './public/js/prod.js'

var pkg = bundle(src,dest);

pkg.then(() => {

    var result = uglifyjs.minify([dest],{
    });


    fs.writeFile(dest,result.code,(err) => {
        if(err){
            throw err;
        }
        console.log("build done!")
    })
}).catch(err => {
    console.log(err);
})